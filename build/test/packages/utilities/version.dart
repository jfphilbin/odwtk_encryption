// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
//library version;

import 'dart:convert';
import 'dart:io';

import 'package:args/args.dart';

/**
 * This class implements Semantic Versioning 2.0.0 (see http://semver.org/).
 *
 * Summary
 *
 * Given a version number MAJOR.MINOR.PATCH, increment the:
 *
 *    MAJOR version when you make incompatible API changes,
 *    MINOR version when you add functionality in a backwards-compatible manner, and
 *    PATCH version when you make backwards-compatible bug fixes.
 *
 * Additional labels for pre-release and build metadata are available as extensions
 * to the MAJOR.MINOR.PATCH format.
 */

class VersionType {
  final String name;
  const VersionType(this.name);

  static const alpha    = const VersionType("alpha");
  static const beta     = const VersionType("beta");
  static const rc       = const VersionType("RC");
  static const released = const VersionType("released");

  static getType(String s) {
    switch (s) {
      case 'alpha': return alpha;
      case 'beta': return beta;
      case 'rc': return rc;
      case 'released': return released;
      //TODO add correct error
      default: throw new Error();
    }
  }

  toString() => 'VersionType.$name';
}

// Pre-Releases are indicated by one of: '-alpha.[n]', '-beta.[n]', '-RC[rc].[n]' or
// 'released', where [n] is the pre-release-edit-number.  This number is incremented
// for each new pre-release version, whether -alpha, -beta or -RC.  The release
// candidates have an [RC] number, which indicates the RC version.
class Version {
  static const String versionFile = 'version.json';

  String package   = null;
   // These three numbers must be positive with no leading zeros
  int    major     = 0;
  int    minor     = 1;
  int    patch     = 0;
  VersionType _type = VersionType.alpha;
  int    edit      = 0;
  int    rcNum     = 0;
  //TODO finish
  String build     = "";

  Version(this.package, this.major, this.minor, this.patch, this._type, this.edit,
          this.rcNum, this.build);

  int get incMajor {major++; return major;}
  int get incMinor {minor++; return minor;}
  int get incPatch {patch++; return patch;}
  void set type(VersionType vType) {_type = vType;}
  int get incEdit {edit++; return edit;}
  int get incRcNum {rcNum++; return rcNum;}

  //TODO this should eventually be some hash code of the package
  String getBuild() => "+0000";

  Map get _map => {'package': package, 'major': major, 'minor': minor, 'patch': patch,
                    'type': _type.name, 'edit': edit, 'rcNum': rcNum, 'build': build};

  static read() {
    File file = new File(versionFile);
    String vf = file.readAsStringSync();
    Map vInfo = JSON.decode(vf);
    VersionType vType = vInfo['type'];
    return new Version(vInfo['package'], vInfo['major'], vInfo['minor'], vInfo['patch'],
                       vType, vInfo['edit'], vInfo['rcNum'], vInfo['build']);
  }

  void write() {
    String vInfo = JSON.encode(_map);
    File file = new File(versionFile);
    file.writeAsStringSync(vInfo);
  }

  toString() {
    switch (_type) {
      case VersionType.released:
        return '$package:$major.$minor.$patch$build';
      case VersionType.alpha:
      case VersionType.beta:
        return '$package:$major.$minor.$patch-${_type.name}.$edit$build';
      case VersionType.rc:
        return '$package:$major.$minor.$patch-${_type.name}-$rcNum.$edit$build';
      default:
        throw new Error();
    }
  }
}

const PACKAGE = 'package';
const MAJOR   = 'major';
const MINOR   = 'minor';
const PATCH   = 'patch';
const TYPE    = 'type';
const EDIT    = 'edit';
const RCNUM   = 'rcnum';
const FILE    = 'file';

ArgResults argResults;

void main(List<String> arguments) {
  exitCode = 0; //presume success
  final parser = new ArgParser()
          ..addFlag(PACKAGE, negatable: false, abbr: 'pkg')
          ..addFlag(MAJOR, negatable: false)
          ..addFlag(MINOR, negatable: false)
          ..addFlag(PATCH, negatable: false)
          ..addFlag(TYPE, negatable:  false)
          ..addFlag(EDIT, negatable:  false)
          ..addFlag(RCNUM, negatable: false)
          ..addFlag(FILE, negatable: false, abbr: 'f');

    argResults = parser.parse(arguments);
    //List<String> paths = argResults.rest;

    String package = argResults[PACKAGE];

    print(package);

}
