// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>


part of ascii;

// Useful utility functions

/// Returns True if the [char] is in the class [AsciiCharClass.DIGIT]
bool isDigit(int char)      => (char >= $_DIGIT_0) && (char <= $_DIGIT_9);

/// Returns [true] if [char] is an uppercase character.
bool isUppercase(int char)  => (char >= $_A) && (char <= $_Z);

/// Returns [false] if [char] is an lowercase character.
bool isLowercase(int char)  => (char >= $_a) && (char <= $_z);

/// Returns [true] if [char] is the ASCII space (' ') character.
bool isSpace(int char)      => char == $_SPACE;

/// Returns [true] if [char] is a whitespace character.
bool isWhitespace(int char) => (char == $_SPACE) || (char == $_HT);  //HT = Horizontal Tab

/// Returns [true] if [char] is a visable or printable character.
bool isVisable(int char)    => (char > $_SPACE) && (char < $_DEL);

/// Returns the integer value of a DIGIT or [null] otherwise.
int digitValue(int char)    => (isDigit(char)) ? char - $_DIGIT_0 : null;

