// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library us_ascii_map;
/**
 * The US ASCII Character Set
 *
 * The codes and strings used in this file are defined in
 * ANSI X3.4-1986 and
 * ISO 646 International Reference Version
 *
 * "Codes 0 through 31 and 127 are unprintable control characters.
 *  Code 32 (decimal) is a non-printing spacing character.  Codes
 *  33 through 126 (decimal) are printable graphic characters."
 * See <http://www.columbia.edu/kermit/ascii.html>.
 *
 * The names and descriptions in the table below are from the
 * report; however, in the report the descriptions are all capital
 * or uppercase letters, and in the usAsciiMap below they are in both
 * upper and lowercase for readability.
 *
 */
Map usAsciiMap = {
    "className":  "Ascii",
    "fieldCount": 6,
    "fieldTypes": ["int","String", "String"],
    "fieldNames": ["code","name","description","type"],
    "values":
      [[0,  "NUL", "Null",                      "Control", ["NULL"]],
       [1,  "SOH", "Start of Header",           "Control"],
       [2,  "STX", "Start of Text",             "Control"],
       [3,  "ETX", "End of Text",               "Control"],
       [4,  "EOT", "End of Transmission",       "Control"],
       [5,  "ENQ", "Enquiry",                   "Control"],
       [6,  "ACK", "Acknowledge",               "Control"],
       [7,  "BEL", "Bell (beep)",               "Control"],
       [8,  "BS",  "Backspace",                 "Whitespace", ["BACKSPACE"]],
       [9,  "HT",  "Horizontal Tab",            "Whitespace", ["HTAB", "TAB"]],
       [10, "LF",  "Line Feed",                 "Whitespace", ["LINEFEED", "NEWLINE"]],
       [11, "VT",  "Vertical Tab",              "Whitespace", ["VTAB"]],
       [12, "FF",  "Form Feed",                 "Whitespace", ["FORMFEED"]],
       [13, "CR",  "Carriage Return",           "Whitespace", ["RETURN"]],
       [14, "SO",  "Shift Out",                 "Control"],
       [15, "SI",  "Shift In",                  "Control"],
       [16, "DLE", "Data Link Escape",          "Control"],
       [17, "DC1", "Device Control 1 (XON)",    "Control"],
       [18, "DC2", "Device Control 2",          "Control"],
       [19, "DC3", "Device Control 3 (XOFF)",   "Control"],
       [20, "DC4", "Device Control 4",          "Control"],
       [21, "NAK", "Negative Acknowledge",      "Control"],
       [22, "SYN", "Synchronous Idle",          "Control"],
       [23, "ETB", "End of Transmission Block", "Control"],
       [24, "CAN", "Cancel",                    "Control"],
       [25, "EM",  "End of Medium",             "Control"],
       [26, "SUB", "Substitute",                "Control"],
       [27, "ESC", "Escape",                    "Control", ["ESCAPE"]],
       [28, "FS",  "File Separator",            "Control"],
       [29, "GS",  "Group Separator",           "Control"],
       [30, "RS",  "Record Separator",          "Control"],
       [31, "US",  "Unit Separator",            "Control"],
       // Punctuation
       [32, "SP",                "Space",              "Whitespace", ["SPACE"]],
       [33, "EXCLAM",  "Exclamation Mark",   "Punctuation", ["EXCLAMATION_MARK", "EXCLAMATION"]],
       [34, "QUOTE",    "Quotation Mark",     "Punctuation", ["QUOTATION_MARK", "QUOTATION"]],
       [35, "NUMBER",       "Number Sign",        "Punctuation", ["NUMBER_SIGN"]],
       [36, "DOLLAR",       "Dollar Sign",        "Punctuation", ["DOLLAR_SIGN"]],
       [37, "PERCENT",      "Percent Sign",       "Punctuation", ["PERCENT_SIGN"]],
       [38, "AMPER",         "Ampersand",          "Punctuation", ["AMPERSAND"]],
       [39, "APOSTR",        "Apostrophe",         "Punctuation", ["APOSTROPHE"]],
       [40, "LParen",  "Left Parentheses",   "Punctuation", ["LEFT_PARENTHESES"]],
       [41, "RPAREN", "Rigth Parentheses",  "Punctuation", ["RIGHT_PARENTHESES"]],
       [42, "STAR",          "Asterisk",           "Punctuation", ["ASTERISK"]],
       [43, "PLUS",         "Plus Sign",          "Punctuation", ["PLUS_SIGN"]],
       [44, "COMMA",             "Comma",              "Punctuation"],
       [45, "MINUS",        "Hyphen, Minus Sign", "Punctuation", ["MINUS_SIGN", "HYPHEN"]],
       [46, "PERIOD",            "Period, Full Stop",  "Punctuation", ["FULL_STOP"]],
       [47, "SLASH",           "Solidus, Slash",     "Punctuation", ["SOLIDUS"]],
       // Digits or Numbers
       [48, "DIGIT_0", "Digit Zero",  "Digit", ["0"]],
       [49, "DIGIT_1", "Digit One",   "Digit", ["1"]],
       [50, "DIGIT_2", "Digit Two",   "Digit", ["2"]],
       [51, "DIGIT_3", "Digit Three", "Digit", ["3"]],
       [52, "DIGIT_4", "Digit Four",  "Digit", ["4"]],
       [53, "DIGIT_5", "Digit Five",  "Digit", ["5"]],
       [54, "DIGIT_6", "Digit Six",   "Digit", ["6"]],
       [55, "DIGIT_7", "Digit Seven", "Digit", ["7"]],
       [56, "DIGIT_8", "Digit Eight", "Digit", ["8"]],
       [57, "DIGIT_9", "Digit Nine",  "Digit", ["9"]],
       // More Punctuation
       [58, "COLON",              "Colon",             "Punctuation"],
       [59, "SEMI",          "Semicolon",         "Punctuation", ["SEMICOLON"]],
       [60, "LESS",     "Less-Than Sign, Left Angle Bracket",    "Punctuation", ["LESS_THAN_SIGN", "LESS_THAN", "LEFT_ANGLE"]],
       [61, "EQUAL",        "Equals Sign",       "Punctuation", ["EQUALS_SIGN"]],
       [62, "GREATER",  "Greater-Than Sign, Rigth Angle Bracket", "Punctuation", ["GREATER_THAN_SIGN", "GREATER_THAN", "RIGHT_ANGLE"]],
       [63, "QUESTION",      "Question Mark",     "Punctuation", ["QUESTION_MARK"]],
       [64, "AT_SIGN", "Commeration At Sign", "Punctuation", ["COMMERCIAL_AT_SIGN", "AT"]],
       // Uppercase Letters
       [65, "A", "Capital Letter A", "Uppercase"],
       [66, "B", "Capital Letter B", "Uppercase"],
       [67, "C", "Capital Letter C", "Uppercase"],
       [68, "D", "Capital Letter D", "Uppercase"],
       [69, "E", "Capital Letter E", "Uppercase"],
       [70, "F", "Capital Letter F", "Uppercase"],
       [71, "G", "Capital Letter G", "Uppercase"],
       [72, "H", "Capital Letter H", "Uppercase"],
       [73, "I", "Capital Letter I", "Uppercase"],
       [74, "J", "Capital Letter J", "Uppercase"],
       [75, "K", "Capital Letter K", "Uppercase"],
       [76, "L", "Capital Letter L", "Uppercase"],
       [77, "M", "Capital Letter M", "Uppercase"],
       [78, "N", "Capital Letter N", "Uppercase"],
       [79, "O", "Capital Letter O", "Uppercase"],
       [80, "P", "Capital Letter P", "Uppercase"],
       [81, "Q", "Capital Letter Q", "Uppercase"],
       [82, "R", "Capital Letter R", "Uppercase"],
       [83, "S", "Capital Letter S", "Uppercase"],
       [84, "T", "Capital Letter T", "Uppercase"],
       [85, "U", "Capital Letter U", "Uppercase"],
       [86, "V", "Capital Letter V", "Uppercase"],
       [87, "W", "Capital Letter W", "Uppercase"],
       [88, "X", "Capital Letter X", "Uppercase"],
       [89, "Y", "Capital Letter Y", "Uppercase"],
       [90, "Z", "Capital Letter Z", "Uppercase"],
       // More Punctuation
       [91,  "LSQUARE",  "Left Square Bracket",  "Punctuation", ["LEFT_SQUARE_BRACKET"]],
       [92,  "BACKSLASH",      "Reverse Solidus (Backslash)", "Punctuation", ["REVERSE_SOLIDUS"]],
       [93,  "RSQUARE", "Right Square Bracket", "Punctuation", ["RIGHT_SQUARE_BRACKET"]],
       [94,  "CIRCOMFLEX",    "Circumflex Accent",    "Punctuation", ["CIRCOMFLEX_ACCENT"]],
       [95,  "LOW_LINE",             "Low Line, Underline, Underscore", "Punctuation", ["_", "UNDERLINE", "UNDERSCORE"]],
       [96,  "GRAVE",         "Grave Accent",         "Punctuation", ["GRAVE_ACCENT"]],
       // Lowercase Letters
       [97,  "a", "Small Letter a",   "Lowercase"],
       [98,  "b", "Small Letter b",   "Lowercase"],
       [99,  "c", "Small Letter c",   "Lowercase"],
       [100, "d", "Small Letter d",   "Lowercase"],
       [101, "e", "Small Letter e",   "Lowercase"],
       [102, "f", "Small Letter f",   "Lowercase"],
       [103, "g", "Small Letter g",   "Lowercase"],
       [104, "h", "Small Letter h",   "Lowercase"],
       [105, "i", "Small Letter i",   "Lowercase"],
       [106, "j", "Small Letter j",   "Lowercase"],
       [107, "k", "Small Letter k",   "Lowercase"],
       [108, "l", "Small Letter l",   "Lowercase"],
       [109, "m", "Small Letter m",   "Lowercase"],
       [110, "n", "Small Letter n",   "Lowercase"],
       [111, "o", "Small Letter o",   "Lowercase"],
       [112, "p", "Small Letter p",   "Lowercase"],
       [113, "q", "Small Letter q",   "Lowercase"],
       [114, "r", "Small Letter r",   "Lowercase"],
       [115, "s", "Small Letter s",   "Lowercase"],
       [116, "t", "Small Letter t",   "Lowercase"],
       [117, "u", "Small Letter u",   "Lowercase"],
       [118, "v", "Small Letter v",   "Lowercase"],
       [119, "w", "Small Letter w",   "Lowercase"],
       [120, "x", "Small Letter x",   "Lowercase"],
       [121, "y", "Small Letter y",   "Lowercase"],
       [122, "z", "Small Letter z",   "Lowercase"],
       // More Punctuation
       [123,  "LBRACE",  "Left Curly Bracket, Left Brace",  "Punctuation", ["LEFT_CURLY_BRACKET", "LBRACE", "LCURLY"]],
       [124,  "VBAR",        "Vertical Line, Vertical Bar",     "Punctuation", ["VERTICAL_LINE", "VLINE", "VERTICAL_BAR"]],
       [125,  "RBRACE", "Right Curly Bracket, Right Brace", "Punctuation", ["RIGHT_CURLY_BRACKET", "RCURLY"]],
       [126,  "TILDE",               "Tilde",               "Punctuation"],
       // One last Control character
       [127,  "DEL",                 "Delete",              "Control", ["DELETE"]]
      ]
    };

List usAsciiSynonyms =
    [
     ["NUL",                 "NULL"],
     ["BS",                  "BACKSPACE"],
     ["HT",                  "HTAB", "TAB"],
     ["LF",                  "LINEFEED", "NEWLINE"],
     ["FF",                  "FORMFEED"],
     ["CR",                  "RETURN"],
     ["ESC",                 "ESCAPE"],
     ["SP",                  "SPACE"],
     ["MINUS_SIGN",          "HYPHEN"],
     ["PERIOD",              "FULL_STOP"],
     ["SOLIDUS",             "SLASH"],
     ["LESS_THAN_SIGN",      "LEFT_ANGLE_BRACKET"],
     ["GREATER_THAN_SIGN",   "RIGHT_ANGLE_BRACKET"],
     ["REVERSE_SOLIDUS",     "BACKSLASH"],
     ["CIRCOMFLEX_ACCENT",   "CARET"],
     ["LOW_LINE",            "UNDERLINE", "UNDERSCORE"],
     ["LEFT_CURLY_BRACKET",  "LEFT_BRACE"],
     ["VERTICAL_LINE",       "VERTICAL_BAR"],
     ["RIGHT_CURLY_BRACKET", "RIGHT_BRACE"],
     ["DEL",                 "DELETE"]
     ];

/**
 *
 * The character Classes are not part of the report, but are defined
 * here for convenience.
 *
 *    Class         Description (decimal)
 *    --------------------------------------------
 *    Control       "NUL" <= code <= "SP" || 127
 *    Whitespace    "BS"  <= code <= "CR" || 32
 *    Digit         "0"   <= code <= "9"
 *    Uppercase     "A"   <= code <= "Z"
 *    Lowercase     "a"   <= code <= "z"
 *    Graphic       "!"   <= code <= "~"
 *    Punctuation   "!"   <= code <= "/" ||
 *                  ":"   <= code <= "@" ||
 *                  "["   <= code <= "`" ||
 *                  "{"   <= code <= "~"
 */
                // Name           Range          Extra
List charClass = [["Control",     [["NUL", "US"]],  "DEL"],
                 ["Whitespace",  [["BS",  "CR"]],  "SP"],
                 ["Digit",       [["0",   "9"]],   ""],
                 ["Uppercase",   [["A",   "Z"]],   ""],
                 ["Lowercase",   [["a",   "z"]],   ""],
                 ["Graphic",     [["!",   "~"]],   ""],
                 ["Punctuation", [["!",  "/"],
                                  [":",  "@"],
                                  ["[",  "`"],
                                  ["{",  "~"]],    ""]
                 ];

String charClassPredicates = """ 
/// Useful Definition
const int \$CRLF = 0x0D0A;
/// Useful Utilities
int digitValue(int ch)    => ch - DIGIT_0;

bool _inRange(int ch, int min, int max) => (min <= ch) && (ch <= max);

/// Character Class Predicates
bool isControl(int ch)     => _inRange(ch, NUL, US);
bool isWhitespace(int ch)  => _inRange(ch, BS, CR) || (ch == SP);
bool isDigit(int ch)       => _inRange(ch, DIGIT_0, DIGIT_9);
bool isUppercase(int ch)   => _inRange(ch, A, Z);
bool isLowercase(int ch)   => _inRange(ch, a, z);
bool isGraphic(int ch)     => _inRange(ch, EXCLAMATION_MARK, DEL);

bool isPunctuation(int ch) => _inRange(ch, EXCLAMATION_MARK, SLASH) ||
                              _inRange(ch, COLON, COMMERCIAL_AT_SIGN) ||
                              _inRange(ch, LEFT_SQUARE_BRACKET, GRAVE_ACCENT) ||
                              _inRange(ch, LEFT_CURLY_BRACKET, TILDE); 
""";
