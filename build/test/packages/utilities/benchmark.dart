// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library benchmark;

class Benchmark {
  final Stopwatch stopwatch = new Stopwatch();
  //TODO how to declare next line as type List<List<String, int>>?
  bool isRunning;
  List _log;

  // Getters
  int  get start => _start();
  int  get stop  => _stop();
  List get log   => _log;

  // Record an instant in time with a name.
  int split(String name) => _addEntry(name);

  //*** private methods ***

  // Always resets the Stopwatch before restarting it.
  int _start()  {
    if (!isRunning) return null;
    _reset();
    int time = _addEntry('start');
    stopwatch.start();
    isRunning = true;
    return time;
  }

  int _stop() {
    if (!isRunning) return null;
    stopwatch.stop();
    isRunning = false;
    return _addEntry('stop');
  }

  // Reset the Stopwatch
  void _reset() {
    _log = new List();
    stopwatch.reset();
  }

  int _addEntry(String name) {
    if (!isRunning) return null;
    int time = stopwatch.elapsedMicroseconds;
    _log.add([name, time]);
    return time;
  }

}