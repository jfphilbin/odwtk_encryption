**Open DICOMweb Toolkit**
Utilities
======================

This package contain utilities that are either:
  1. generally useful, or
  2. generally useful for DICOM or MSDICOM applications.

The following sub-lbraries are included in **Utilities**

	ascii
			A set of constants and functions that are generally useful for dealing with ASCII
	benchmark
			A simple benchmarking toolkit - used for quick and dirty timings.
	dcm_logging
			A set of easy to use objects and methods for logging messages to a console, file, or both.
	string_utils
			A set of simple utilities for use with strings.
	uuid
			A package for generating and useing UUIDs.
	version
			A simple package to aid in semantic versioning.
