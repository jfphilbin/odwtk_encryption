// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library abstract_cipher;

import 'dart:typed_data';

/**
 * DICOMweb Toolkit Cipher
 *
 * This exposes the necessary functionality to utilize the Web Crypto library
 * in browsers.
 */

/**
 * Adapter Constructor
 * <br/>The adapter class exposes the AES-GCM implementation, a few methods
 * to adapt the implementation to MSD requirements, wrapped zlib compression,
 * generic test cases, and benchmarking. All parameters of the constructor
 * can be left as null.
 *
 * We don't preallocate arrays because in JS we don't need to.
 * http://coding.smashingmagazine.com/2012/11/05/writing-fast-memory-efficient-javascript/
 *
 * @constructor
 * @class adapter
 *
 * @param {Boolean} benchmark Benchmarking reports for compression, encryption
 *                            and decryption.
 * @param {String} codec Just so we don't have to assume what the data format will be
 *                       when its passed in, you can simply pass in a codec to cast
 *                       toBits or it will be assumed that you are passing in a hex
 *                       string. Note that acceptable codecs are: sjcl_arrayBuffer,
 *                       arrayBuffer, hex, base64, base64url, utf8String.
 * //TODO why not make "data" Uint8List
 * @param {String} data You can pass a reader object on construction. For now let us
 *                      assume that it is a cleaned up base64 encoded string. We might
 *                      also assume that it contains our iv, aad, and tag.
 * //TODO why not make key Uint8List
 * @param {String} key The key is expected to be a string that will be converted to
 *                     bits.
 * //TODO Uint8List
 * @param {String} iv The iv can be null, passed as a string, or parsed from data
 *                    later on.
 * //TODO Uint8List
 * @param {String} aad The aad can be null, passed as a string, or parsed from
 *                     data later on.
 * //????
 * @param {String} tag The tag can be null, passed as a string, or parsed from
 *                     data later on.
 */

/*
 * AbstractCipher
 *
 * An interface for creating Cryptographic Ciphers.
 */
abstract class AbstractCipher {
  static const int    IV_LENGTH  = 12;       //?? what should this be?
  static const int    MAC_LENGTH = 16;       // ?? is 128bits enough?
  static const String NAME       = 'AES_GCM_256';
  static const int    KEY_LENGTH = 32;       // ?? what is key length?
  // These can be methods or fields


  //TODO should any of these fields be final
  Uint8List secret;          // Secret used to create the key
  Uint8List aad;             // Additional Authenticated Data
  Uint8List uint8Buffer;    // Data to be encryted or decrypted
  int       userDataStart;  // The offset in the buffer where user data begins
  int       userDataLength; // The length of the user data
  Uint8List key;            // encryption key
  Uint8List iv;             // Initialization Vector
  Uint8List mac;            // Message Authentication Code

  String    get name;

  // Returns the user writable part of the buffer
  Uint8List get userBuffer;

  Uint8List generateKey([Uint8List secret]);
  Uint8List importKey([Uri uri]);            // Get a key from the uri
  Uint8List generateIV([Uint8List iv]);
  Uint8List encrypt();       //
  Uint8List decrypt();

}