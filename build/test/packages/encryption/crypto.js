/**
 * @fileOverview MSD JavaScript AES-GCM Adapter
 *
 * This adapter class will expose the necessary functionality to utilize the
 * SJCL AES-GCM implementation, the modified web worker AES-GCM implementation,
 * wrapped zlib compression, and execution-time based benchmarking. For the
 * JASMINE experimental pbkdf/decrypt, dont assume benchmarking.
 *
 * TODO: Replace object prototype checks for ArrayBuffer with a function,
 * finish encryption method wrapper, finish prepend, allow prf to be switched.
 *
 * @author Michael Rushanan <micharu123@gmail.com>
 */

/**
 * Adapter Constructor
 * <br/>The adapter class exposes the AES-GCM implementation, a few methods
 * to adapt the implementation to MSD requirements, wrapped zlib compression,
 * generic test cases, and benchmarking. All parameters of the constructor
 * can be left as undefined.
 *
 * We don't preallocate arrays because in JS we don't need to.
 * http://coding.smashingmagazine.com/2012/11/05/writing-fast-memory-efficient-javascript/
 *
 * @constructor
 * @class adapter
 *
 * @param {Boolean} benchmark Benchmarking reports for compression, encryption
 *                            and decryption.
 * @param {String} codec Just so we don't haver to assume what the data format will be
 *                       when its passed in, you can simply pass in a codec to cast
 *                       toBits or it will be assumed that you are passing in a hex
 *                       string. Note that acceptable codecs are: sjcl_arrayBuffer,
 *                       arrayBuffer, hex, base64, base64url, utf8String.
 * @param {String} data You can pass a reader object on construction. For now let us
 *                      assume that it is a cleaned up base64 encoded string. We might
 *                      also assume that it contains our iv, aad, and tag.
 * @param {String} key The key is expected to be a string that will be converted to
 *                     bits.
 * @param {String} iv The iv can be undefined, passed as a string, or parsed from data
 *                    later on.
 * @param {String} aad The aad can be undefined, passed as a string, or parsed from
 *                     data later on.
 * @param {String} tag The tag can be undefined, passed as a string, or parsed from
 *                     data later on.
 */

msd.adapter.aes_gcm = function (benchmark, codec, data, key, iv, aad, tag, experimental) {

  // These can all be undefined, we handle that assumption in all methods.
  this.benchmark = benchmark;
  this.experimental = experimental;

  switch (codec) {
    case 'sjcl_arrayBuffer':
        // Modified SJCL only.
        this.codec = sjcl.codec.arrayBuffer;
        break;
    case 'hex':
        this.codec = sjcl.codec.hex;
        break;
    case 'base64':
        this.codec = sjcl.codec.base64;
        break;
    case 'base64url':
        this.codec = sjcl.codec.base64url;
        break;
    case 'utf8String':
        this.codec = sjcl.codec.utf8String;
        break;
      case 'arrayBuffer':
        this.codec = new Uint8Array();
        break;
    case 'bitArray':
    default:
        this.codec = undefined;
        break;
  }

  // When allowing these to be undefined, we have to actually check.
  if(data) {
    if(!this.codec) {
      this.data = data;
    } else {
      if(Object.prototype.toString.call(this.codec)== "[object Uint8Array]"){
        // We'll want a Uint8Array passed in.
        this.data = new Uint8Array(data);
      } else {
          this.data = this.codec.toBits(data);
        }
    }
  }
  if(key) {
    if(!this.codec) {
      this.key = key;
    } else {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.key = new Uint8Array(key);
      } else {
        this.key = this.codec.toBits(key);
      }
    }
  }
  if (iv) {
    if(!this.codec) {
      this.iv = iv;
    } else {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.iv = new Uint8Array(iv);
      } else {
        this.iv = this.codec.toBits(iv);
      }
    }
  }
  if (aad) {
    if(!this.codec) {
      this.aad = aad;
    } else {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.aad = new Uint8Array(aad);
      } else {
        this.aad = this.codec.toBits(aad);
      }
    }
  }
  if (tag) {
    if(!this.codec) {
      this.tag = tag;
    } else {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.tag = new Uint8Array(tag);
        // Length in bytes.
        this.tag_length = tag.length;
      } else {
        this.tag = this.codec.toBits(tag);
        this.tag_length = sjcl.bitArray.bitLength(tag);
      }
    }
  }

  // These will be useful for later comparison functions.
  this.ciphertext = [];
  this.plaintext = [];

  // Not all browsers support performance.now. So, lets make a member variable that
    // points to whatever is acceptable.
  try {
    this.performance = performance;
  } catch(e) {
    this.performance = Date;
  }

  // Check for the experimental flag. If it's set, we will try for nfcrypto plugin,
  // or microsofts window.crypto/window.msCrypto as IE 11 supports AES-GCM.
    if(experimental){
        if((!this.codec || Object.prototype.toString.call(this.codec)=="[object Uint8Array]") ||
           (!this.data  || Object.prototype.toString.call(this.data)=="[object Uint8Array]")
          ) {
          // Do nothing.
        } else {
          console.warn("Warning: The experimental flag is for W3C Draft Web Cryptography use only");
          this.experimental = false;
        }

        // Keep a member variable for plugin and windows.
        this.plugin = false;
        this.winIE = false;

        // Try Blocks.
        try {
          this.cryptoSubtle = nfCrypto.subtle;
          this.plugin = true;
        } catch(e) {
            console.warn("Warning: NfWebCrypto Plugin is not found");
            try {
            this.cryptoSubtle = crypto.subtle;
          } catch(e) {
            console.warn("Warning: Crypto Subtle is not found");
            try {
              this.cryptoSubtle = msCrypto.subtle;
              this.winIE = true;
            } catch(e) {
              console.warn("Warning: Microsoft Crypto Subtle is not found");

              // Tried them all, set experimental to false.
              console.error("Error: W3C Draft Web Cryptography not supported");
              this.experimental = false;
            }
          }
        }
    }

  // Lets bake the GLOBAL SECRET and IMPLEMENTATION_VERSION_NAME in here for now.
    if(!this.experimental) {
        this.GLOBAL_SECRET = sjcl.codec.hex.toBits("000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F");
  } else {
        this.GLOBAL_SECRET = new Uint8Array([0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F]);
  }
    this.IMPLEMENTATION_VERSION_NAME = "MSD 1.0.0";
};

msd.adapter.aes_gcm.prototype = {
  /**
   * getBenchmark
   * <br/>Return the benchmark boolean value.
   * @returns {Boolean} Benchmark value.
   */
  getBenchmark:function() {
    return this.benchmark;
  },

  /**
   * setBenchmark
   * <br/>Set the benchmark boolean value.
   * @param {Boolean} benchmark
   */
  setBenchmark:function(benchmark) {
    this.benchmark = benchmark;
  },

  /**
   * getData
   * <br/>Return the data object.
   * @returns {bitArray} The data object has already been converted
   *                     to a bitArray, so return that.
   */
  getData: function () {
    return this.data;
  },

  /**
   * setData
   * <br/>Set the data object, this will convert it to the structure per codec.
   * @param {String} data
   */
  setData:function(data) {
    if(this.codec) {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.data = new Uint8Array(data);
      } else {
        this.data = this.codec.toBits(data);
      }
    } else {
      // TODO: Verify bitArray was passed in.
      this.data = data;
    }
  },

  /**
   * getKey
   * <br/>Return the bitArray key.
   * @returns {bitArray}
   */
  getKey:function() {
    return this.key;
  },

  /**
   * setKey
   * <br/>Set the key to the structure per codec of passed string value key.
   * @param {String} key
   */
  setKey:function(key) {
    if(this.codec) {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.key = new Uint8Array(key);
      } else {
        this.key = this.codec.toBits(key);
      }
    } else {
      this.key = key;
    }
  },

  /**
   * getPlaintext
   * <br/>Return the private member plaintext.
   * @return {BitArray} plaintext
   */
  getPlaintext:function() {
    return this.plaintext;
  },

  /**
   * setPlaintext
   * <br/>Set the plaintext to the structure per codec of passed string value
    * plaintext.
   * @param {BitArray} plaintext
   */
  setPlaintext:function(plaintext) {
    if(this.codec) {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.plaintext = new Uint8Array(plaintext);
      } else {
        this.plaintext = this.codec.toBits(plaintext);
      }
    } else {
      this.plaintext = plaintext;
    }
  },

  /**
   * getCiphertext
   * <br/>Return the private member ciphertext.
   * @return {BitArray} ciphertext
   */
  getCiphertext:function() {
    return this.ciphertext;
  },

  /**
   * setCiphertext
   * <br/>Set the ciphertext to the structure per codec of passed string value
   * plaintext.
   * @param {BitArray} cipertext
   */
  setCiphertext:function(ciphertext) {
    if(this.codec) {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.ciphertext = new Uint8Array(ciphertext);
      } else {
        this.ciphertext = this.codec.toBits(ciphertext);
      }
    } else {
      this.ciphertext = ciphertext;
    }
  },

  /**
   * getIV
   * <br/>Return the private member iv.
   * @return {BitArray} iv
   */
  getIV:function() {
    return this.iv;
  },

  /**
   * setIV
   * <br/>Set the iv to the structure per codec of passed iv.
   * @param {BitArray} iv
   */
  setIV:function(iv) {
    if(this.codec) {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.iv = new Uint8Array(iv);
      } else {
        this.iv = this.codec.toBits(iv);
      }
    } else {
      this.iv = iv;
    }
  },

  /**
   * getAAD
   * <br/>Return the private member aad.
   * @return {BitArray} aad
   */
  getAAD:function() {
    return this.aad;
  },

  /**
   * setAAD
   * <br/>Set the aad to the structure per codec of passed aad.
   * @param {BitArray} aad
   */
  setAAD:function(aad) {
    if(this.codec) {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.aad = new Uint8Array(aad);
      } else {
        this.aad = this.codec.toBits(aad);
      }
    } else {
      this.aad = aad;
    }
  },

  /**
   * getTAG
   * <br/>Return the private member tag.
   * @return {BitArray} tag
   */
  getTag:function() {
    return this.tag;
  },

  /**
   * setTag
   * <br/>Set the tag to the passed bitArray.
   * @param {BitArray} tag
   */
  setTag:function(tag) {
    if(this.codec) {
      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]"){
        this.tag = new Uint8Array(tag);
      } else {
        this.tag = this.codec.toBits(tag);
      }
    } else {
      this.tag = tag;
    }

    this.tag = tag;
  },

  /**
   * getExperimental
   * <br/>Return the private member experimental. We currently do not allow
   * experimental to be set after instantiation.
   * @return {Boolean} experimental
   */
  getExperimental:function() {
    return this.experimental;
  },

    /**
     * string to uint array
     * </br>String to Uint8Array. A useful helper function provided by the NfWebCrypto
     * polyfill found here: https://github.com/Netflix/NfWebCrypto.
     * @param {String} s
     * @return {UInt8Array} ua
     */
    text2ua:function(s) {
        var ua = new Uint8Array(s.length);
        for (var i = 0; i < s.length; i++) {
            ua[i] = s.charCodeAt(i);
        }
        return ua;
    },

    /**
     * ua2text
     * </br>Uint8Array to string. A useful helper function provided by the NfWebCrypto
     * polyfill found here: https://github.com/Netflix/NfWebCrypto.
     * @param {UInt8Array} ua
     * @return {String} s
     */
    ua2text:function(ua) {
        var s = '';
        for (var i = 0; i < ua.length; i++) {
            s += String.fromCharCode(ua[i]);
        }
        return s;
    },

    /**
     * ua2hex
     * </br>Uint8Array to hex.
     * @param {UInt8Array} ua
     * @return {String} s
     */
    ua2hex:function(ua) {
        var h = '';
        for (var i = 0; i < ua.length; i++) {
            h += "\\0x" + ua[i].toString(16);
        }
        return h;
    },

    /**
     * parseTypedArray
     * <br/>This helper method will parse HTTP Multi-part MIME messages. Specifically,
     * it will strip out \n\r and the mutli-part header from the MSD metadata. This
     * is necessary because these character encodings in Uint8Array are represented
     * in the array, thus changing the offset size. It only works on this.data.
   */
  parseTypedArray:function() {
    // We assume 500 bytes to be on the safe side. The between variable checks
    // for values between 0 and 35 (or ^@ and # characters). E.g., byte values
    // 13,10 represent a carriage return or ^M. We assume bytes of interest
    // are not interleaved.
    var tmp = new Uint8Array(this.data.subarray(0,500));
    var between_dash = false;
    var between_pound = false;
    var throwout = {'start':[],'finish':[]};

    for(var i=0;i<tmp.length;i++) {
      // i == 45 or "-"
      if(tmp[i] == 45) {
        // Look a head for match.
        if(tmp[i+1] == 45) {
          between_dash = true;
        }
      }
      // i == 67 or "C"
      else if(tmp[i] == 67 && between_dash) {
        between_dash = false;
        throwout.start.push(i);
      }
      // Parsing out carriage returns, ^M.
      // i == ^M
      else if(tmp[i] == 13 && tmp[i+1] == 10) {
        // Ignore these if already between.
        if(!between_dash && !between_pound && tmp[i+2] != 13) {
          throwout.finish.push(i);
          if(tmp[i+2] != 0) {
            //+1 worked but not really.
            throwout.start.push(i+2);
          }
        }
      }
      // Parsing out ^@ to #.
      // i == ^@
      else if(tmp[i] == 0) {
        between_pound = true;
      }
      // i == #
      else if(tmp[i] == 35 && between_pound) {
        between_pound = false;
        // By going back one you should get the return.
        throwout.start.push(i-1);
      }
    }

    // Clean up the data arraybuffer.
    var retArray = undefined;
    for(var i=0;i<throwout.start.length;i++) {
      var tmp_subarray;
      if(throwout.finish[i]) {
        tmp_subarray = new Uint8Array(tmp.subarray(throwout.start[i],throwout.finish[i]));
      } else {
        // We'd expect throwout.finish to be start.length-1.
        tmp_subarray = new Uint8Array(tmp.subarray(throwout.start[i]));
      }

      // Keep a 'concatenated' ArrayBuffer.
      if(!retArray) {
        retArray = new Uint8Array(tmp_subarray);
      } else {
        var newArray = new Uint8Array(retArray.byteLength+tmp_subarray.byteLength);
        newArray.set(new Uint8Array(retArray),0);
        newArray.set(new Uint8Array(tmp_subarray), retArray.byteLength);
        retArray = new Uint8Array(newArray);
      }
    }

    // The last thing we have to do is replace those first 500 bytes with our
    // smaller retArray and 'concatenate' that to everything after those 500
    // bytes for our resultant member ArrayBuffer.
    var data_tail = new Uint8Array(this.data.subarray(500))
    var data_parsed = new Uint8Array(retArray.byteLength+data_tail.byteLength);
    data_parsed.set(new Uint8Array(retArray), 0);
    data_parsed.set(new Uint8Array(data_tail), retArray.byteLength);
    this.data = new Uint8Array(data_parsed);
  },

  /**
   * parseMetadata
   * <br/>This helper method will strip out the useful metadata and return an object
   * of that metadata.
   *
   * @param {String} data_utf8 This optional parameter allows you to pass in a data
   *                           object that is UTF8 encoded and thus will simply be
   *                           parsed without any conversions. If undefined, this.data
   *                           will be converted and parsed.
   * @param {Integer} aad_size An additional optional parameter that allows you to
   *                           specify an aad of any size. The default size will be
   *                           2048 bits (256 bytes).
   *
   * @return {Object} metadata This object will contain a set of values expressing
   *                           compression, siuid, uuid, and length.
   */
  parseMetadata:function(data_utf8, aad_length) {
    if(!aad_length) {
      aad_length = 2048;
    }

    if(!data_utf8 && !(Object.prototype.toString.call(this.codec)=="[object Uint8Array]")) {
      var tmp_aad;
      var tmp_aad_values;

      // Well since no aad exists yet, lets slice it out of the data bitArray!
      if(!this.aad) {
        tmp_aad = sjcl.bitArray.bitSlice(this.data, 0, aad_length);
        tmp_aad = this.base64decode(sjcl.codec.base64.fromBits(tmp_aad)).out;
        tmp_aad_values = tmp_aad.split(/\n/g);
      } else {
        tmp_aad = this.base64decode(sjcl.codec.base64.fromBits(this.aad)).out;
        tmp_aad_values = tmp_aad.split(/\n/g);
      }
    // We're using ArrayBuffers, so lets call subarray up to aad_length, convert to a string and parse.
    } else if(!data_utf8 && (Object.prototype.toString.call(this.codec)=="[object Uint8Array]")) {
      tmp_aad = new Uint8Array(this.data.subarray(0,(aad_length/8)));
            tmp_aad = this.ua2text(tmp_aad);
            tmp_aad_values = tmp_aad.split(/\n/g);
    } else {
      // Data already in UTF8, just parse it.
      tmp_aad_values = data_utf8.split(/\n/g);
    }

    // Because of that extra bit metadata, we need to adjust these values.
    var metadata = {
      compressed:'',
      siuid:'',
      uuid:'',
      length:''
    };

    // Validate where first field is, then trim.
    for(var i=0;i<tmp_aad_values.length;i++) {
      try {
        if(tmp_aad_values[i].split('=')[0] == 'compressed') {
          metadata.compressed = (tmp_aad_values[i].split('=')[1]).trim();
        } else if(tmp_aad_values[i].split('=')[0] == 'siuid') {
          metadata.siuid = (tmp_aad_values[i].split('=')[1]).trim();
        } else if(tmp_aad_values[i].split('=')[0] == 'uuid') {
          metadata.uuid = (tmp_aad_values[i].split('=')[1]).trim();
        } else if(tmp_aad_values[i].split('=')[0] == 'length') {
          metadata.length = (tmp_aad_values[i].split('=')[1]).trim();
        }
      } catch(e) {
        // We want to ignore TypeErrors that result from an
        // undefined passed to trim (string op).
      }
    }

    return metadata;
  },

  /**
   * _HMACSHA256_importKey
   * <br/>This private method calls the NfWebCrypto import function of a raw
   * key. It will then chain together other private method calls that complete
   * the PBKDF process, resulting in an exported key that will be divided.
     *
   * @private
   * @param {String} KeyMessage The input into the HMAC-SHA256.
   * @param {Function} callback The callback allows a function to be passed in
   *                            for experimental SHA256 use, which happens to
   *                            be asynchronous. The callback should expect a
   *                            time parameter if you want to run a benchmark.
   * @param {Float} time This value should represent round-trip flight of PBKDF.
   */
   _HMACSHA256_importKey:function(keyMessage, callback, time) {

      // Horrific fix for these asynchronous calls that lose member var scope.
    var cryptoSubtle = this.cryptoSubtle;

      if(this.plugin || this.winIE) {
        var _sign = this._sign;

        var importOp = this.cryptoSubtle.importKey("raw", this.GLOBAL_SECRET, {
              name: "HMAC",
            hash: "SHA-256"
          },
            true,
          ["sign", "verify"]
          );

        importOp.oncomplete = function (e) {
              var hmacKey = e.target.result;
              _sign(hmacKey, keyMessage, callback, time, cryptoSubtle);
        };

        importOp.onerror = function (e) {
            console.log("Import RAW key ERROR", e);
        };

      // Web Cryptography API Browser Supported. Note that the signature happens here.
      } else {
        cryptoSubtle.importKey("raw", this.GLOBAL_SECRET, {
            name: "HMAC",
            hash: {name: "SHA-256"}
          }, true, ["sign","verify"])
        .then(function(key) {
          return cryptoSubtle.sign({
              name: "HMAC",
              hash: {name: "SHA-256"}
            }, key,
            keyMessage);})
      .catch(function(error) {
          console.error("HMAC signOp failed", error);
      })
        .then(function(signedData) {
                  var signature = new Uint8Array(signedData);
                  var time_ns = undefined;
                  var time_ms = (performance.now()-time);
                  var key = new Uint8Array(signature.valueOf().subarray(0,(signature.valueOf().length/2)));
                  callback(key, time_ms);})
        .catch(function(error) {
          console.error("Callback operation failed", error);
      });
      }
   },

  /**
   * _sign
   * <br/>This private method calls the NfWebCrypto import function of a raw
   * key. It will then chain together other private method calls that complete
   * the PBKDF process, resulting in an exported key that will be divided.
     *
   * @private
   * @param {Object} hmacKey The output of private _import method.
   * @param {String} KeyMessage The input into the HMAC-SHA256.
   * @param {Function} callback The callback allows a function to be passed in
   *                            for experimental SHA256 use, which happens to
   *                            be asynchronous. The callback should expect a
   *                            time parameter if you want to run a benchmark.
   * @param {Float} time This value should represent round-trip flight of PBKDF.
   */
   _sign:function(hmacKey, keyMessage, callback, time, cryptoSubtle) {
        var signOp = cryptoSubtle.sign({
          name: "HMAC",
          hash: "SHA-256"
        },
        hmacKey,
        keyMessage
    );

      signOp.oncomplete = function (e) {
          var signature = new Uint8Array(e.target.result);
          var time_ns = undefined;
          // TODO: Pass benchmark check in.
            var time_ms = (performance.now()-time);

          var key = new Uint8Array(signature.valueOf().subarray(0,(signature.valueOf().length/2)));

          // The callback should expect two parameters, time and key.
          callback(key, time_ms);
      };

      signOp.onerror = function (e) {
        console.log("HMAC sign ERROR", e);
      };
   },

  /**
   * pbkdf
   * <br/>The password based key derivation function for MSD simply uses an
   * HMAC SHA256 over the concatenation of GLOBAL_SECRET and studyInstanceUID.
   *
   * @param {String} studyInstanceUID This is a required field for deriving a
   *                                  key. Note that this method will replace
   *                                  the current member key.
   * @param {Function} callback The callback allows a function to be passed in
   *                            for experimental SHA256 use, which happens to
   *                            be asynchronous. The callback should expect a
   *                            time parameter if you want to run a benchmark.
   * @return {Object} .benchmark If benchmark is set to true, then we can
   *                             return the benchmark execution time.
   */
  pbkdf:function(studyInstanceUID, callback) {
    // Define our keyMessage as MSD does.
    var keyMessage = this.IMPLEMENTATION_VERSION_NAME + studyInstanceUID;
    // TODO: If the above siuid isn't provided, then you might want to create a member variable that has it?
    var key, half_key;

    // Using HTML5 performance.now() to do benchmarking timing.
    var start, time_ns;
    if(this.benchmark) {
      start = performance.now();
    }

      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
        if(this.experimental) {
          // Private import key that will chain HMAC and export
          // of key to a raw format.
          this._HMACSHA256_importKey(this.text2ua(keyMessage), callback, start);
        } else {
          // We will use the asmCrypto library for this section if using ArrayBuffers but
          // not experimental.
          // TODO: This is wrong.
          //key = new asmCrypto.HMAC_SHA256.hex(this.GLOBAL_SECRET, this.text2ua(keyMessage));
          //this.key = key.subarray(0,(key.length/2));
        }
      } else {
        key = (new sjcl.misc.hmac(this.GLOBAL_SECRET, sjcl.hash.sha256)).encrypt(keyMessage);
        // Only want half the bits.
        half_key = sjcl.bitArray.bitLength(key)/2;
        this.key = sjcl.bitArray.bitSlice(key, 0, half_key);
      }

    if(this.benchmark) {
      time_ns = (performance.now()-start);

      // Return benchmark time so we can do other things with it other than report
          // to console.
      return { benchmark:time_ns };
    }
  },

  /**
   * _AESGCM_importKey
   * <br/>Private method that setups import key for NfWebCrypto plugin's asynch
   * execution.
   *
   * @private
   * @param {Function} callback The callback allows a function to be passed in
   *                            for experimental AES-GCM use, which happens to
   *                            be asynchronous. The callback should expect a
   *                            time parameter if you want to run a benchmark.
   * @param {Float} time This value should represent round-trip flight of PBKDF.
   */
  _AESGCM_importKey:function(callback, time) {

      // Horrific fix for these asynchronous calls that lose member var scope.
      var cryptoSubtle = this.cryptoSubtle;
      var ciphertext = this.ciphertext;
      var iv = this.iv;
      var aad = this.aad;
      var tag_length = this.tag_length;

      if(this.plugin) {
        var _AESGCM_decrypt = this._AESGCM_decrypt;

        var op = this.cryptoSubtle.importKey("raw",
          this.key,
          {
            name: "AES-GCM"
          },
          true,
          ["encrypt", "decrypt"]
        );
        op.onerror = function (e) {
            console.log("Import RAW Key failed.", e.target.result);
        };
        op.oncomplete = function (e) {
            var aesgcm_key = e.target.result;
            _AESGCM_decrypt(callback, aesgcm_key, ciphertext, iv, aad, tag_length, time, cryptoSubtle);
        };

      // Web Cryptography API Browser Supported. Note that decryption happens here too. Tag length
      // needs to be in bits!!
      } else {
        cryptoSubtle.importKey("raw", this.key, {
            name: "AES-GCM"
          }, true, ["encrypt","decrypt"])
        .then(function(key) {
          return cryptoSubtle.decrypt({
              name: "AES-GCM",
              iv: iv,
              additionalData: aad,
              tagLength: tag_length
            }, key,
            ciphertext);
        })
      .catch(function(error) {
          console.error("Decryption failed", error);
      })
        .then(function(plaintext) {
            var time_ms = undefined;
                  var time_ms = (performance.now()-time);
                  callback(plaintext, time_ms);
            })
      .catch(function(error) {
          console.error("Callback failed", error);
      });
      }

  },

  /**
   * _AESGCM_decrypt
   * <br/>The password based key derivation function for MSD simply uses an
   * HMAC SHA256 over the concatenation of GLOBAL_SECRET and studyInstanceUID.
   *
   * @param {Object} key The output of _AESGCM_importKey.
   * @param {Uint8Array} ciphertext Local copy of the member ciphertext.
   * @param {Uint8Array} iv Local copy of the member iv.
   * @param {Uint8Array} aad Local copy of the member aad.
   * @param {Integer} tag_length Local copy of the member tag_length.
   * @param {Function} callback The callback allows a function to be passed in
   *                            for experimental AES-GCM use, which happens to
   *                            be asynchronous. The callback should expect a
   *                            time parameter if you want to run a benchmark.
   * @param {Float} time This value should represent round-trip flight of PBKDF.
   */
  _AESGCM_decrypt:function(callback, key, ciphertext, iv, aad, tag_length, time, cryptoSubtle) {
    var decryptOp = cryptoSubtle.decrypt(
      {
        name: "AES-GCM",
        iv: iv,
        additionalData: aad,
        tagLength: tag_length
      },
      key,
      ciphertext
    );
      decryptOp.onerror = function (e) {
        console.log("Decryption failed.", e.target.result);
      };
      decryptOp.oncomplete = function (e) {
        var plaintext = e.target.result;
      var time_ns = (performance.now()-time);
      callback(plaintext, time_ns);
      };
  },

  /**
   * decrypt
   * <br/>Decrypt in AES-GCM mode with respect to the MSD format. This method simply
   * wraps sjcl.aes_gcm and NfWebCrypto. The result of this execution will set the
   * member plaintext. If you want the plaintext, call the getPlaintext method.
   *
   * @param {Boolean} dont_overwrite If you have set values such as IV and AAD via
   *                                 setter methods, you can choose to not overwrite
   *                                 these values by setting this to true.
   * @param {Integer} aad_length This integer represents the aad length in bits.
   * @param {Integer} iv_length This integer represents the iv length in bits.
   * @param {Integer} tag_length This integer represents the tag_length in bits.
   * @param {Function} callback The callback allows a function to be passed in
   *                            for experimental SHA256 use, which happens to
   *                            be asynchronous. The callback should expect a time
   *                            parameter if you want to run a benchmark.
   * @return {Object} .benchmark If benchmark is set to true, then we can return the
   *                             benchmark execution time.
   */
  decrypt: function (dont_overwrite, aad_length, iv_length, tag_length, callback) {
    var start, end, time_ns;
    if(this.benchmark) {
      var start = this.performance.now();
    }

      if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
        this.preprocess(dont_overwrite, aad_length, iv_length, tag_length);
        if(this.experimental) {
          // Private import key that will chain to the decryption call. We have to pass member
          // variables because async call will go out of scope.
          this._AESGCM_importKey(callback, start);
        } else {
          console.log('Not yet implemented');
        }
      } else {
        var aes = new sjcl.cipher.aes(this.key);

        // Using preprocess to parse member data into an acceptable format.
        this.preprocess(dont_overwrite, aad_length, iv_length, tag_length);

        // Set the plaintext member to the result of the decryption.
        this.plaintext = sjcl.mode.gcm.decrypt(aes, this.ciphertext, this.iv, this.aad, this.tag_length);
      }

    if(this.benchmark) {
      if(!this.experimental){
        end = this.performance.now();
      }
      time_ns = (end-start);

      // Return benchmark time so we can do other things with it other than reporting
            // to console.
      return {benchmark:time_ns};
    }
  },

  /**
   * preprocess
   * <br/>This method is necessary to parse the data into the AAD, IV, Ciphertext
   * + Tag. As it is currently, the SJCL AES-GCM implementation parameterizes
   * AAD and IV while parsing Ciphertext and Tag from data.
   *
   *
   * @param {Integer} aad_length This integer represents the aad length in bits.
   * @param {Integer} iv_length This integer represents the iv length in bits.
   * @param {Integer} tag_length This integer represents the tag_length in bits.
   */
  preprocess:function(dont_overwrite, aad_length, iv_length, tag_length) {
      // Useful for performing bitArray operations.
    var w = sjcl.bitArray;

    // If aad_length not provided, check to see if member aad has been set.
    // If not, we will set the aad_length to 256bytes.
    if(!aad_length) {
      if(!this.add) {
        if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
          aad_length=(2048/8);
        } else {
          // In bits. 1952bits without IV.
          aad_length = 2048;
        }

      } else {
        if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
            aad_length = this.aad.length;
        } else {
          aad_length = w.bitLength(this.aad);
        }
      }
    }

    // If iv_length not provided, check to see if member iv has been set.
    // If not, we will set the iv_length to 12bytes.
    if(!iv_length) {
      if(!this.iv) {
        if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
          iv_length = (96/8);
        } else {
          iv_length = 96;
        }
      } else {
        if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
          iv_length = this.iv.length;
        } else {
          iv_length = w.bitLength(this.iv);
        }
      }
    }

    // If tag_length not provided, check to see if member tag has been set.
    // If not, we will set the tag_length to 16bytes.
    if(!tag_length) {
      if(!this.tag) {
        if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
          tag_length = 128;
        } else {
          tag_length = 128;
        }
      } else {
        if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
          tag_length = this.tag.length;
        } else {
          tag_length = w.bitLength(this.tag);
        }
      }
    }

    var curr_data = this.data, curr_length, curr_aad, curr_iv, curr_tag;

    if(Object.prototype.toString.call(this.codec)=="[object Uint8Array]") {
      curr_length = this.data.length;
      curr_aad = curr_data.subarray(0, aad_length);
      // useful for debugging.
      //console.log(btoa(String.fromCharCode.apply(null, curr_aad)));
      curr_iv = curr_aad.subarray(aad_length-iv_length);
      curr_data = curr_data.subarray(aad_length);
      curr_tag = curr_data.subarray(curr_length-tag_length);
    } else {
      // Lets make a local copy of the bitArray member data and get the length.
      curr_length = w.bitLength(curr_data);

      // Slice off the first 256 bytes for the AAD and IV.
      curr_aad = w.bitSlice(curr_data, 0, aad_length);
      curr_iv = w.bitSlice(curr_aad, aad_length-iv_length);

      // Now remove the AAD from curr_data, so slice from aad_length to the end.
      // TODO: Exception if this.data is undefined, thus this would fail.
      curr_data = w.bitSlice(curr_data, aad_length);
      curr_tag = w.bitSlice(curr_data, curr_length-tag_length);
    }

    // If ciphertext is undefined and dont_overwrite is false, then set the value. If the
    // ciphertext is undefined and dont_overwrite is true, set the value anyway. If both
    // ciphertext is undefined and dont_overwrite is false, set the value.
    if((this.ciphertext && !dont_overwrite) || (!this.ciphertext && dont_overwrite) || (!this.ciphertext && !dont_overwrite)) {
      this.ciphertext = curr_data;
    }

    if((this.iv && !dont_overwrite) || (!this.iv && dont_overwrite) || (!this.iv && !dont_overwrite)) {
      this.iv = curr_iv;
    }

    if((this.aad && !dont_overwrite) || (!this.aad && dont_overwrite) || (!this.aad && !dont_overwrite)) {
      this.aad = curr_aad;
    }

    if((this.tag_length && !dont_overwrite) || (!this.tag_length && dont_overwrite) || (!this.tag_length && !dont_overwrite)) {
      this.tag_length = tag_length;
    }

    if((this.tag && !dont_overwrite) || (!this.tag && dont_overwrite) || (!this.tag && !dont_overwrite)) {
      this.tag = curr_tag;
    }
  },

  /**
   * prepend
   * <br/>Like the preprocess method, prepend modifies the encryption output
   * a bit. Specifically, following the MSD specification we need to prepend the
   * AAD+IV to Data+Tag. Something like this: AAD||IV||DATA||TAG.
   * @param {BitArray} ciphertext This will be the concatenation of data and tag.
   * @return {BitArray} ciphertext This will be a bit array of the format
   *                               described above.
   */
  prepend:function(aad, iv, tag_length) {
    // Using members aad, iv.
    var a = sjcl.bitArray;

    if(!aad) {
      //console.log("No AAD provided, falling back to no AAD.");
      this.aad = [];
    }

    if(!iv) {
      //console.log("No IV provided, falling back to no IV.");
      this.iv = [];
    }

    if(!tag_length) {
      this.tag_length = 128;
    } else {
      this.tag_length = tag_length;
    }

    var curr_aad = a.concat(this.aad, this.iv);
    this.ciphertext = a.concat(curr_aad, this.data);
  },

  /**
   * deflateZLIB
   * <br/>Helper method that wraps zpipe compress (to smaller).
   * @param {Object} compressed_blob The compressed_blob can be whatever, but it
   *                 will most likely be a compressed file.
   * @return {Object} .out:deflated_blob
   *                  .benchmark:time_ns If benchmark is set to true, then we can
   *                                     return the benchmark execution time.
   */
  deflateZLIB:function(blob) {
    if(!this.benchmark) {
      // Do not run the benchmark.
      return {out:zpipe.deflate(blob)};
    } else {
      var start = this.performance.now();
      var deflated_blob = zpipe.deflate(blob);
      var end = this.performance.now();
      var time_ns = (end-start);
      //console.log('Execution time of deflation: ' + time_ns + 'ns');

      // Return benchmark time so we can do other things with it other than reporting
            // to console.
      return {out:deflated_blob, benchmark:time_ns};
    }
  },

  /**
   * inflateZLIB
   * <br/>Helper method that wraps zpipe decompress (to normal).
   * @param {Object} blob The blob can be whatever, but it will most likely be a file.
   * @return {Object} .out:inflated_blob
   *                  .benchmark:time_ns If benchmark is set to true, then we can
   *                                     return the benchmark execution time.
   */
  inflateZLIB:function(blob) {
    if(!this.benchmark) {
      return {out:zpipe.inflate(blob)};
    } else {
      var start = this.performance.now();
      var inflated_blob = zpipe.inflate(blob);
      var end = this.performance.now();
      var time_ns = (end-start);

      // Return benchmark time so we can do other things with it other than reporting
            // to console.
      return {out:inflated_blob, benchmark:time_ns};
    }
  },

  /**
   * base64encode
   * <br/>Helper method to base64 encode strings.
   * @param {Object} blob The blob can be whatever, but it will most likely be a file.
   * @return {Object} .out:retVal A base64 encoded string.
   *                  .benchmark:time_ns If benchmark is set to true, then we can
   *                                     return the benchmark execution time.
   */
  base64encode:function(blob) {
    var retVal;
    if(!this.benchmark) {
          // IE does not support btoa, but Gecko and Webkit do.
      try {
          if(!window.btoa) {
            retVal = btoa(blob);
          } else {
            retVal = window.btoa(blob);
          }
      } catch(e) {
        retVal = btoa(blob);
      }
        return {out:retVal};

        } else {
      var start = this.performance.now();
      try {
          if(!window.btoa) {
            retVal = btoa(blob);
          } else {
            retVal = window.btoa(blob);
          }
      } catch(e) {
        retVal = btoa(blob);
      }
      var end = this.performance.now();
      var time_ns = (end-start);

      // Return benchmark time so we can do other things with it other than reporting
            // to console.
      return {out:retVal, benchmark:time_ns};
      }
  },

  /**
   * base64decode
   * <br/>Helper method to base64 decode strings. Also, you can specify whether the
   * string to decode came from the HTML5 TextAsDataURL. If so, we will truncate the
   * b64 prefix.
   * @param {Object} blob The blob can be whatever, but it will most likely be a file.
   * @return {Object} .out:retVal A base64 decoded string.
   *                  .benchmark:time_ns If benchmark is set to true, then we can
   *                                     return the benchmark execution time.
   */
  base64decode:function(blob, fileAPI) {
    if(fileAPI) {
      var tmp = blob.split('base64,');
      blob = tmp[1];
      //console.log(blob);
    }

    if(!this.benchmark) {
      // IE does not support btoa, but Gecko and Webkit do.
      var retVal;
      try {
        if(!window.atob) {
          retVal = atob(blob);
        } else {
          retVal = window.atob(blob);
        }
      } catch (e) {
        retVal = atob(blob);
      }
      return {out:retVal};

        } else {
      var start = this.performance.now();
      try {
        if(!window.atob) {
          retVal = atob(blob);
        } else {
          retVal = window.atob(blob);
        }
      } catch(e) {
        retVal = atob(blob);
      }
      var end = this.performance.now();
      var time_ns = (end-start);

      // Return benchmark time so we can do other things with it other than reporting
            // to console.
      return {out:retVal, benchmark:time_ns};
      }
  }
};