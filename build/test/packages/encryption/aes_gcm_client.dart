// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library aes_gcm_client;

import 'dart:typed_data';

import 'package:encryption/aes_gcm_cipher.dart';
/**
 * AesGcm256 Encryption for Web Clients (browser based)
 *
 * The Client and Server have to use different encryption libraries because the
 * Client accesses the encoder/decoder from the browers, via window.crypto.subtle.
 *
 */
class AesGcmClient extends AesGcmCipher {

  // Constructor
  AesGcmClient(aad, userDataLength, [iv, secret = NORMAL_SECRET])
      : super(aad, userDataLength, iv, secret);

  @override
  Uint8List encrypt() {
    log.fine('Start Encrypt');
    benchmark.start;
    //TODO finish - Call window.crypto.subtle

    benchmark.stop;
    log.fine('End Encrypt');
    return fullBuffer;
  }

  @override
  Uint8List decrypt() {
    log.fine('Start Decrypt');
    benchmark.start;
  //TODO finish - Call window.crypto.subtle

    benchmark.stop;
    log.fine('End Decrypt');
    return fullBuffer;
  }
}