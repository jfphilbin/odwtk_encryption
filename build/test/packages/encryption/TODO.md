TODO List for encryption package
  1. Figure out how to call window.crypto.subtle
  2. Can we use pub.dartlang.org/Cipher.dart for key and iv generation?
  3. Decide if Math.Random is good enough RNG, and Please check out the random.dart file
     and let me know what you think of these RNGs.
  4. Can Mike add GCM mode to the Cipher package?
  5. Can we generate the secret from the UID?  So the secret would change for each study?
  6. Look at the TODO notes in the 


When encryption is finished and tested 
  1. Delete all unnecessary files:
      a. abstract_cipher.dart
      b. crypto.dart
      c. crypto.js
      d. random.dart  ???