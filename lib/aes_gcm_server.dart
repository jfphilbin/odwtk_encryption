// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library aes_gcm_server;

import 'dart:typed_data';
import 'package:encryption/aes_gcm_cipher.dart';

/**
   * AesGcm256 Encryption for Web Servers
   *
   * The Client and Server have to use different encryption libraries because the
   * Client accesses the encoder/decoder from the browsers, via window.crypto.subtle;
   * while the server needs a seperate library.
   *
   */
//TODO figure out where we can get serverside code for AES_GCM

class AesGcmServer extends AesGcmCipher {

  // Constructor
  AesGcmServer(aad, userDataLength, [iv, secret = NORMAL_SECRET])
      : super(aad, userDataLength, iv, secret);

  @override
  Uint8List encrypt() {
    log.fine('Start Encrypt');
    benchmark.start;
    //TODO finish - Call server-side library

    benchmark.stop;
    log.fine('End Encrypt');
    return fullBuffer;
  }

  @override
  Uint8List decrypt() {
    log.fine('Start Decrypt');
    benchmark.start;
    //TODO finish - Call server-side library

    benchmark.stop;
    log.fine('End Decrypt');
    return fullBuffer;
  }

}
