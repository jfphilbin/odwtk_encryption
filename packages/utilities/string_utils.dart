// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library string_utils;

import 'package:logging/logging.dart';

final log = new Logger("utilities.string_utils");

bool inRange(int val, int min, int max) => (min <= val) && (val <= max);

/**
 * Returns a hex [String] with specified [padLeft] padding and leading "0x", if
 * [prefix] is true.
 */
String intToHex(int i, [int padLeft = 0, bool prefix = true]) {
  String s = i.toRadixString(16);
  s = s.padLeft(padLeft, "0");
  return (prefix) ? s = "0x" + s : s;
}

/// Returns a [List] of hex [Strings] mapped from [list]
Iterable<String> intListToHex(List<int>   list) => list.map(intToHex);

/// Returns a [String] in the form of a [List] ("[
String hexListToString(list) => "[" + list.join(", ") + "]";

/// /// A function that maps an [int> to a [String] in [fmt] format
typedef String IntToString(int i);

/// A function that maps a [List<int>] to a [String].
typedef String IntListToString(List<int> l);

/// A function that maps a [List<int>] to a [String] in [format].
IntListToString intListToString(IntToString format) {
   return (List<int> list) { return  "[" + list.map(format).join(", ") + "]"; };
   }

/** Returns a [String] in [List<HexString>] format. */
IntListToString intListToHexString = intListToString(intToHex);

/** Returns a [String] that corresponsed to 'name' of the [Symbol]. */
String symbolToString(Symbol sym) {
  String s = sym.toString();  // Symbol("foo")
  return s.substring(8, s.length - 2);
}

