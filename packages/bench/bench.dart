library bench;

import 'dart:async';
import 'dart:mirrors';
import 'package:unittest/unittest.dart';

part 'src/matcher.dart';
part 'src/meta.dart';
part 'src/mock.dart';
part 'src/reflect_tests.dart';
part 'src/test_driver.dart';
part 'src/unittest_driver.dart';

final noop = ([_]){};

expectOneEvent(Stream s) => s.listen(expectAsync(noop));
expectTwoEvents(Stream s) => s.listen(expectAsync(noop, count: 2));
expectNEvents(Stream s, int n) => s.listen(expectAsync(noop, count: n));
expectDone(Stream s) => s.listen(noop, onDone: expectAsync(noop));
expectNoEvents(Stream s) => s.listen((_) => fail('Unexpected call'));
expectThen(Future f) => f.then(expectAsync(noop));
