// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
import 'dart:html';
import 'dart:typed_data';
import 'random.dart';
import 'aes_gcm.dart';

void main() {  
  querySelector("#random_text_id")
    ..onClick.listen(getRandom);
  
  querySelector("#sha256_text_id")
    ..onClick.listen(getSHA256);
  
  querySelector("#aesgcmE_text_id")
    ..onClick.listen(getAESGCM_E);

  querySelector("#aesgcmD_text_id")
    ..onClick.listen(getAESGCM_D);
}

void getRandom(MouseEvent event) {
  var text = querySelector("#random_text_id").text;
  var buffer = cryptoRNG();
  querySelector("#random_text_id").text = buffer.toString();
}

void getSHA256(MouseEvent event) {
  var text = querySelector("#sha256_text_id").text;
  var buffer = "Not Implemented";
  querySelector("#sha256_text_id").text = buffer.toString();
  
  // TODO write a hash class
  var hasher = new AES_GCM();
  Uint8List SECRET = new Uint8List.fromList(
    [0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,
    0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,
    0x1C,0x1D,0x1E,0x1F]);
  hasher.generateKey(SECRET,"001");
}

void getAESGCM_E(MouseEvent event) {
  var cipher = new AES_GCM();
  cipher.encrypt();
}

void getAESGCM_D(MouseEvent event) {
  var text = querySelector("#aesgcm_text_id").text;
  var buffer = "Not Implemented";
  querySelector("#aesgcm_text_id").text = buffer.toString();
}