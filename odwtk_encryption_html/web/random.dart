// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library random;
import 'dart:typed_data';
import 'dart:html';

/**
 * Cryptographically-secure random number generator exposed by the browser.
 */
int cryptoRNG() {
  final rnd_number = new Uint32List(1);
  window.crypto.getRandomValues(rnd_number);
  
  // Range index error for anything than base index.
  return rnd_number[0];
}