// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library aes_gcm;

/**
 * AES Galois Counter Mode (GCM) Cipher
 *
 * TODO finish documentation
 */

import 'random.dart';
import 'dart:math';
import 'dart:typed_data';
import 'dart:html';
import 'dart:js';
import 'dart:async';
import 'package:logging/logging.dart';
import 'package:bench/bench.dart';

// TODO secret is used for development only.
// TODO this should be const.
const tmp = const[0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,
  0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,
  0x1C,0x1D,0x1E,0x1F]; 
final Uint8List DEV_SECRET = new Uint8List.fromList(tmp);

class AES_GCM {
  static const int        IV_LENGTH  = 12; // Bytes.
  static const int        MAC_LENGTH = 16; // Bytes.
  static const String     NAME       = 'AES_GCM_256';
  static const String     IMPLEMENTATION_VERSION_NAME = '';
  static const int        KEY_LENGTH = 32; // Bytes.
  final        Logger     log = new Logger('AESGCM256');

  // TODO Should these be private fields
  Uint8List _fullBuffer;     // The full buffer, [add, iv, userDate, mac]. of Data to
                            // be encryted or decrypted.
  Uint8List _userData;       //
  int       _userDataOffset; // The offset in the uint8uffer where user data begins
  int       _userDataLength; // The length of the user data
  Uint8List _secret;         // The secret used to generate the Key
  Uint8List _aad;            // Additional Authenticated Data
  Uint8List _key;            // encryption Key
  Uint8List _iv;             // Initialization Vector (IV)
  Uint8List _mac;            // Message Authentication Code (MAC)

  // TODO what is the type of _subtle
  final _subtle = window.crypto.subtle;
  // final _SubtleCrypto subtle;
  
  // TODO iv and secret are only optional
  /** 
   * Constructor. Pass in additional authenticated data, data length, iv,
   * and a secret. 
   */
  AES_GCM([this._aad, this._userDataLength, this._iv, this._secret=DEV_SECRET]) {
    // TODO set default parameter to DEV_SECRET when const is fixed.
    //this._secret = DEV_SECRET;
    
    this._key = generateKey(_secret);

    if (_iv == null) _iv = generateIV(_iv);
    
    _userDataOffset = _aad.length + AES_GCM.IV_LENGTH;
    
    int totalLength = _userDataOffset + _userDataLength + AES_GCM.MAC_LENGTH;
    _fullBuffer = new Uint8List(totalLength);
    
    _userData = _fullBuffer.buffer.asUint8List(_userDataOffset, _userDataLength);
    
    log.fine('New instance created');
  }

  /**
   * Getters and Setters. 
   */
  String get name => NAME;
  String get version => AES_GCM.IMPLEMENTATION_VERSION_NAME;
  int    get iv_length => AES_GCM.IV_LENGTH;
  int    get mac_length => AES_GCM.MAC_LENGTH;
  int    get key_legnth => AES_GCM.KEY_LENGTH;
  
  /**
   * Returns a new [key] based on [secret]. 
   */
  Uint8List generateKey([Uint8List secret, String studyInstanceUID]) {
    log.fine('Entering generateKey');
    //benchmark.split;
    Uint8List key = new Uint8List(AES_GCM.KEY_LENGTH);

    print(AES_GCM.IMPLEMENTATION_VERSION_NAME);
    String keyMessage = AES_GCM.IMPLEMENTATION_VERSION_NAME + studyInstanceUID;
    
    Future future = _subtle.importKey("raw", DEV_SECRET, {
        name: "HMAC", 
        hash: {name: "SHA-256"}
      }, true, ["sign","verify"]);
    
    future.then((key) {
      window.console.debug("import key successful");
      _subtle.sign({
          name: "HMAC", hash: {
              name: "SHA-256"
          }
      }, key, keyMessage);});
 
    future.catchError((error) { print("HMAC signOp failed");});
    
    future.then((signedData) {
      Uint8List signature = signedData;
      Uint8List key = signature.sublist(0,(signature.length ~/ 2));
    });
    
    future.catchError((error) { print("Callback failed.");});
    
    //benchmark.split;
    log.fine('Exiting Key = $key');
    return key;
  }

  // TODO: importSymmetricKey.
  Uint8List importKey([Uri uri]) {
    log.fine('Start importKey');
    //benchmark.split;
    //TODO finish
    key = uri;

    //benchmark.split;
    log.fine('Exiting importKey key = $key');
    return key;
  }

  Uint8List generateIV([Uint8List iv]) {
    if (iv != null) return iv;
    else {
      log.fine('Start generateIV');
      //benchmark.split;

      iv = new Uint8List(IV_LENGTH);
      ByteData bd = iv.buffer.asByteData();
      for(int i = 0; i < 3; i++) {
        int value = generator.nextInt((1<<32) - 1);
        int offset = i * 4;
        bd.setInt32(offset, value);
      }
      log.info('bd = $bd, iv = $iv');
      iv = iv;

      //benchmark.split;
      log.fine('Exiting generateIV iv = $iv');
      return iv;
    }
  }

  Uint8List encrypt() {
    log.fine('Start Encrypt');
    
    // We can probably use Dart's interoperating w/ JS via objects.
    var object = new JsObject(context['widnow.crypto']);
    object['subtle'] = 
        object.callMethod('encrypt', ["AES-GCM", window.crypto.getRandomValues(new Uint8List(16)), window.crypto.getRandomValues(new Uint8List(256)), 128]);

    /*
    window.crypto.subtle.encrypt(
        {
            name: "AES-GCM",

            //Don't re-use initialization vectors!
            //Always generate a new iv every time your encrypt!
            iv: window.crypto.getRandomValues(new Uint8Array(16)),

            //Additional authentication data (unsure what proper use for this is)
            additionalData: window.crypto.getRandomValues(new Uint8Array(256)),
            tagLength: 128,
            };
        },
        key, //from generateKey or importKey above
        data //ArrayBuffer of data you want to encrypt
    )
    .then(function(encrypted){
        //returns an ArrayBuffer containing the encrypted data
        console.log(new Uint8Array(encrypted));
    })
    .catch(function(err){
        console.error(err);
    });*/
    
    log.fine('End Encrypt');
    return fullBuffer;
  }

  Uint8List decrypt() {
    log.fine('Start Decrypt');
    /*window.crypto.subtle.decrypt(
        {
            name: "AES-GCM",
            iv: ArrayBuffer(16), //The initialization vector you used to encrypt
            additionalData: ArrayBuffer, //The addtionalData you used to encrypt
            tagLength: 128, //The tagLength you used to encrypt
        },
        key, //from generateKey or importKey above
        data //ArrayBuffer of the data
    )
    .then(function(decrypted){
        //returns an ArrayBuffer containing the decrypted data
        console.log(new Uint8Array(decrypted));
    })
    .catch(function(err){
        console.error(err);
    });*/
    log.fine('End Decrypt');
    return fullBuffer;
  }
}
