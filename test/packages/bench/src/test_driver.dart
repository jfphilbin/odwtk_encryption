part of bench;

abstract class TestDriver {    
  void add(TestGroup testGroup);
  void run(Duration timeout);
}
