// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library random;

import 'dart:math' as Math;
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:cipher/cipher.dart';

// The two functions below were copied from the pub.dartlang.org/uuid.dart
// package by Yulian Kuncheff.

/**
 * Math.Random()-based RNG. All platforms, fast, not cryptographically strong.
 */
Uint8List mathRNG() {
  int nBytes = 16;
  Uint8List result = new Uint8List(nBytes);
  Math.Random rng = new Math.Random();
  int rand;

  for (var i = 0; i < 16; i++) {
    if ((i & 0x03) == 0) {
      rand = (rng.nextDouble() * 0x100000000).floor().toInt();
    }
    result[i] = rand >> ((i & 0x03) << 3) & 0xff;
  }
  return result;
}

/**
 * AES-based RNG. All platforms, unknown speed, cryptographically strong (theoretically)
 */
Uint8List cryptoRNG() {
  int nBytes = 32;
  Uint8List pwBytes = new Uint8List(nBytes);

  SHA256 hasher = new SHA256();
  Uint8List bytes = mathRNG();
  hasher.add(bytes);
  pwBytes = new Uint8List.fromList(hasher.close().sublist(0, nBytes));

  var params = new KeyParameter(pwBytes);
  var cipher = new BlockCipher("AES")..init(true, params);

  var plainText = pwBytes;
  var cipherText = new Uint8List(cipher.blockSize);
  cipher.processBlock(plainText, 0, cipherText, 0);
  return cipherText.toList();
}
