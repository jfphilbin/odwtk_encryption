// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: Mike Rushanan <micharu123@gmail.com>,
//          James F Philbin <james.philbin@jhmi.edu>
library aes_gcm_cipher;

//TODO
// 1. Figure out how to call window.crypto.subtle
// 2. Should we use Cipher or Subtle for key generation
// 3. Is the random number generation for IV good enough?
/**
 * AES-256 Galois Counter Mode (GCM) Cipher
 *
 * TODO finish documentation
 */

// external imports
import 'dart:math';
import 'dart:typed_data';
import 'dart:math' as Math;
//TODO decide if either of the next two are needed?
import 'package:crypto/crypto.dart';
import 'package:cipher/cipher.dart';

// dwtk imports
import 'package:utilities/benchmark.dart';
import 'package:logging/logging.dart';

// Secret used for development and debugging
const Uint8List NORMAL_SECRET =
    const [0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,
           0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,
           0x1C,0x1D,0x1E,0x1F];
/**
 *TODO documentation
 */

//TODO I've eliminated the dependency on abstract_cipher - we don't need it unless
//     we're implementing other ciphers.
class AesGcmCipher {
  //TODO Mike are these lengths correct?
  static const int    IV_LENGTH  = 12;
  static const int    MAC_LENGTH = 16;
  static const String NAME       = 'AES_GCM_256';
  static const int    KEY_LENGTH = 32;

  //TODO get a better random number generator?
  static final Random     generator   = new Math.Random(0);
  final        Logger     log = new Logger('AesGcm256Cipher');
  final        Benchmark  benchmark = new Benchmark();

  //TODO Should these be private fields
  Uint8List fullBuffer;     // The full buffer, [add, iv, userDate, mac]. of Data to
                            // be encryted or decrypted.
  Uint8List userData;       //
  int       userDataOffset; // The offset in the uint8uffer where user data begins
  int       userDataLength; // The length of the user data
  Uint8List secret;         // The secret used to generate the Key
  Uint8List aad;            // Additional Authenticated Data
  Uint8List key;            // encryption Key
  Uint8List iv;             // Initialization Vector (IV)
  Uint8List mac;            // Message Authentication Code (MAC)

  //TODO what is the type of _subtle
  var       _subtle; //TODO add comment

  // Getters and Setters
  String    get name => NAME;

  // Constructor
  AesGcmCipher(this.aad, this.userDataLength,
               [this.iv, this.secret = NORMAL_SECRET]) {
    key = generateKey(secret);
    if (iv == null) iv = generateIV(iv);
    userDataOffset = aad.length + AesGcmCipher.IV_LENGTH;
    int totalLength = userDataOffset + userDataLength + AesGcmCipher.MAC_LENGTH;
    fullBuffer = new Uint8List(totalLength);
    userData = fullBuffer.buffer.asUint8List(userDataOffset, userDataLength);
    log.fine('New instance created');
  }

  /**
   * Returns a new [key] based on [secret].
   */
  Uint8List generateKey([Uint8List secret]) {
    log.fine('Entering generateKey');
    benchmark.split;
    Uint8List key = new Uint8List(KEY_LENGTH);
    //TODO finish
    //     I think there are key generators in Cipher.dart which

    benchmark.split;
    log.fine('Exiting Key = $key');
    return key;
  }

  //TODO should this be importKey or importSecret?
  Uint8List importKey([Uri uri]) {
    log.fine('Start importKey');
    benchmark.split;
    //TODO finish


    benchmark.split;
    log.fine('Exiting importKey key = $key');
    return key;
  }

  Uint8List generateIV([Uint8List iv]) {
    if (iv != null) return iv;
    else {
      log.fine('Start generateIV');
      benchmark.split;

      iv = new Uint8List(IV_LENGTH);
      ByteData bd = iv.buffer.asByteData();
      for(int i = 0; i < 3; i++) {
        int value = generator.nextInt((1<<32) - 1);
        int offset = i * 4;
        bd.setInt32(offset, value);
      }
      log.info('bd = $bd, iv = $iv');
      iv = iv;

      benchmark.split;
      log.fine('Exiting generateIV iv = $iv');
      return iv;
    }
  }

  Uint8List encrypt() {
    log.fine('Start Encrypt');
    benchmark.start;
    //TODO finish - Call window.crypto.subtle

    benchmark.stop;
    log.fine('End Encrypt');
    return fullBuffer;
  }

  Uint8List decrypt() {
    log.fine('Start Decrypt');
    benchmark.start;
  //TODO finish - Call window.crypto.subtle

    benchmark.stop;
    log.fine('End Decrypt');
    return fullBuffer;
  }

}
