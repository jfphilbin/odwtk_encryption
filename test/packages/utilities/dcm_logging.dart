// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library jfp_logging;

import 'package:logging/logging.dart';
import 'package:logging_handlers/server_logging_handlers.dart';

// Time format for log messages
const DATE_TIME_FORMAT = " HH:mm:ss.SSS";
/// Format for a log message that does not contain an exception.
const PRINT_MESSAGE_FORMAT = "%t %n [%p]:  %m";
const FILE_MESSAGE_FORMAT  = "%t %n [%p]:  %m";

StringTransformer transformer = new StringTransformer(messageFormat: FILE_MESSAGE_FORMAT,
                                                      timestampFormat: DATE_TIME_FORMAT);

void startLogging(String filename, {level: Level.INFO, hierarchical: true}) {

  bool hierarchicalLoggingEnabled = hierarchical;
  Logger.root.level = level;

  var loggerStream = Logger.root.onRecord.asBroadcastStream();
  loggerStream.listen(new LogPrintHandler(messageFormat: PRINT_MESSAGE_FORMAT,
                                          timestampFormat: DATE_TIME_FORMAT));
  loggerStream.listen(new SyncFileLoggingHandler(filename, transformer: transformer));

}
