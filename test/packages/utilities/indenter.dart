// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library indenter;

/**
 * A utility for creating nested indentation.
 */
class Indenter {
  int start;
  int _count;
  int increment;

  Indenter({this.start: 0, this.increment: 2}) {
    this._count = start;
  }

  int    get count           => _count;
  String get indent          => sp;
  String get sp              => "".padRight(_count);
  int    get incr            => _count += increment;
  int    get decr            => _count -= increment;
  void   set add(int i)      {_count += i;}
  void   set subtract(int i) {_count -= i;}

  toString() {
    String spaces = '                                                                  ';
    return spaces.substring(0, count);
  }

  void reset() {_count = start;}
}